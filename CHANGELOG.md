# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).
We adhere loosely to Semantic Versionning as this isn't technically a software dependency, and some manifests might be updated while other not.


## [Unreleased]


## [1.6.0] - 2024-11-26
### Fixed
- Earthly manifests: Add Earthfile for [lume](https://lume.land)


## [1.5.1] - 2024-09-20
### Fixed
- Earthly manifests (node): fix saving lockfile when using pnpm (thanks @bfamchon1!)


## [1.5.0] - 2024-04-08
### Fixed
- Next: fix +build on Next.js serving stale content (CACHE only `.next/cache`, not full `.next/`)

### Added
- Earthly manifests: add Eleventy


## [1.4.4] - 2024-03-08
### Fixed
- Jekyll: remove `CACHE .jekyll-cache` as it breaks `bundle exec ...`
- Next: remove `SAVE ARTIFACT` on cacheDir as it breaks +build (and isn't really useful)

### Changed
- Jekyll: +dev: use +jekyll-base image instead of jekyll/jekyll:4
- Astro: remove `SAVE ARTIFACT` on cacheDir for consistency


## [1.4.3] - 2024-03-08
### Changed
- Earthly manifests: set up CACHE for all generators' incremental build cache dir
- Earthly manifests: by default, don't use a different YAMA_ENV between envs


## [1.4.2] - 2024-03-05
### Changed
- Earthly manifests: upgrade to VERSION 0.8
- Earthly manifests: add a YAMA_ENV env var
- Gitlab CI manifest: remove unecessary `earthly bootstrap`

### Fixed
- Earthly manifests: fix `+deploy-preview` robots.txt being uploaded before the bucket is emptied


## [1.4.1] - 2023-10-18
### Fixed
- Fixed astro's devserver being unreachable


## [1.4.0] - 2023-10-18

### Fixed
- Fix the `+template` target breaking on python 3.12

### Added
- Earthly manifests: add Astro


## [1.3.0] - 2023-09-18

### Changed
- Earthly manifests: renamed target `+devserver` to `+dev`.

### Added
- Earthly manifests: added target `+stop-dev` to stop the devserver.


## [1.2.5] - 2023-09-12

### Fixed
- Github CI manifest: Fixed names for preview branch rules.


## [1.2.4] - 2023-09-12

### Fixed
- CI manifests: Removed the JSON payload's trailing comma.
- Github CI manifest: Fixed $OS_PASSWORD being defined twice.


## [1.2.3] - 2023-09-08

### Fixed
- CI manifests: Fixed the curl payload's heredoc indentation.
- Gitlab CI manifest: Install curl before trying to use it.
- Github CI manifest: Fixed the `on:` keyword and `if:` rules.

### Changed:
- Silence curl outputs.


## [1.2.2] - 2023-09-08

### Fixed
- gitlab CI manifest: Fixed webhook URL.


## [1.2.1] - 2023-09-08

### Fixed
- CI manifest: Fixed webhook URL, HTTP verb, and payload key names.


## [1.2.0] - 2023-09-07

### Added
- CI manifests: notify the Yama-CMS platform about deploy status via webhooks.
- CI: Added smoke tests for swift upload.

### Changed
- Deploy targets are now `+deploy-production` and `+deploy-preview`. Preview deploy is one single target.
- Renamed job/workflow names for coherence.

### Fixed
- Github manifest: Fixed syntax for secrets.
- Github manifest: Updated branch names.


## [1.1.0] - 2023-08-29

### Added
- A changelog!

### Fixed
- `+upload-site` target: figure out files to upload by their size and hash instead of size and date (632cf94).
- `+upload-site` target: delete only the files from the bucket that do not exist locally, instead of deleting the entire bucket's content (a974daa).


## [1.0.1] - 2023-07-24

### Changed
- Next.js Earthfile: Update to Next.js [v13.3](https://nextjs.org/blog/next-13-3#static-export-for-app-router) (remove `next export`) (2b75f2a).
- Gitlab manifest: Use the `earthly/earthly:latest` image instead of pinning a version.
- Github manifest: use the `earthly/actions-setup` action.
