# Functions (User-defined commands) to run package-manager-agnostic installs
NODE_INSTALL_CI:
    FUNCTION
    # This is from https://github.com/vercel/next.js/blob/84531c5301474f9e872ad6db5689a76fe82d7df4/examples/with-docker/Dockerfile#L9-L14
    # We look for the lockfile of each package managers and run the related command.
    # "CI" and "--frozen-lockfile" mean that the lockfile is not modified at all. To update dependencies, use the `+update` target.
    RUN if [ -f yarn.lock ]; then yarn --frozen-lockfile; \
        elif [ -f package-lock.json ]; then npm ci; \
        elif [ -f pnpm-lock.yaml ]; then yarn global add pnpm && pnpm i --frozen-lockfile; \
        else echo "Lockfile not found." && exit 1; \
        fi
NODE_INSTALL:
    FUNCTION
    # same as above, except we don't use "ci" / "frozen lockfile" mode
    RUN if [ -f yarn.lock ]; then yarn; \
        elif [ -f package-lock.json ]; then npm install; \
        elif [ -f pnpm-lock.yaml ]; then yarn global add pnpm && pnpm i; \
        else echo "Lockfile not found." && exit 1; \
        fi



# Internal target that builds a base environment with dependencies.
node-base:
    FROM node:lts-alpine
    WORKDIR /workdir

    CACHE --persist ./node_modules

    COPY package.json ./
    COPY --if-exists yarn.lock ./
    COPY --if-exists package-lock.json ./
    COPY --if-exists pnpm-lock.yaml ./

    DO +NODE_INSTALL_CI

    SAVE ARTIFACT node_modules
