/** NOTE:
 * NOTE: these files are bundled as raw text strings via this webpack-encore configuration:
 * ```
 *   .addRule({
 *       test: /assets\/js\/react\/CIEditor\/templates\/.*\/(Earthfile|\.earthlyignore)$/,
 *       type: "asset/source"
 *   })
 *   .addRule({
 *       test: /assets\/js\/react\/CIEditor\/templates\/.*.yaml/,
 *       type: "asset/source"
 *   })
 * ```
 */
import CIManifest_github_earthly from "./github-earthly.yaml"
import CIManifest_gitlab_earthly from "./gitlab-earthly.yaml"

import DiyEF from "./do-it-yourself/Earthfile"
import DiyEI from "./do-it-yourself/.earthlyignore"
import HugoEF from "./hugo/Earthfile"
import HugoEI from "./hugo/.earthlyignore"
import JekyllEF from "./jekyll/Earthfile"
import JekyllEI from "./jekyll/.earthlyignore"
import NextjsEF from "./nextjs/Earthfile"
import NextjsEI from "./nextjs/.earthlyignore"
import NuxtjsEF from "./nuxtjs/Earthfile"
import NuxtjsEI from "./nuxtjs/.earthlyignore"
import GatsbyEF from "./gatsby/Earthfile"
import GatsbyEI from "./gatsby/.earthlyignore"


const CIManifests = new Map([
    ["github", CIManifest_github_earthly],
    ["gitlab", CIManifest_gitlab_earthly],
]);

export const getCIManifest = (manifestName) => {
    let manifest = CIManifests.get(manifestName)

    if (manifest)
        return manifest
    else
        throw new Errory("YAMA ERROR: the CI manifest you requested isn't in the CIManifests map!")
}

const EarthlyManifests = new Map([
    ["",       { earthfile: DiyEF,    earthlyignore: DiyEI    }],
    ["hugo",   { earthfile: HugoEF,   earthlyignore: HugoEI   }],
    ["jekyll", { earthfile: JekyllEF, earthlyignore: JekyllEI }],
    ["nextjs", { earthfile: NextjsEF, earthlyignore: NextjsEI }],
    ["nuxt",   { earthfile: NuxtjsEF, earthlyignore: NuxtjsEI }],
    ["gatsby", { earthfile: GatsbyEF, earthlyignore: GatsbyEI }],
]);

export const getEarthlyManifest = (generatorName) => {
    if (!generatorName)
    return EarthlyManifests.get("");

    let manifest = EarthlyManifests.get(generatorName);

    if (manifest)
        return manifest;
    else
        throw new Error("YAMA ERROR: the generator manifest you requested isn't in the EarthlyManifests map!");
}
