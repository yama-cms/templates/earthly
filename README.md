# Yama CMS Earthly templates
This repository holds Earthly manifests used for building static websites in CIs and locally using various Static Site Generators (SSGs: Jekyll, Gatsby, Hugo, Nextjs...), along with CI manifests for a few CI systems that we maintain (Gitlab-CI, Github Actions). Templates are under the MIT License.

We might change the manifests if we add new features to Yama CMS, or if new versions of CI systems or SSGs require changes. We [keep a Changelog](./CHANGELOG.md), and we plan to have patches and a notification system so that developers can easily update their CI manifests in the future.

## Relation to the Yama CMS project
This repository contains the templates files used by Yama CMS when initializing a new static site. This repository is our "source of truth": it is kept up-to-date, and the final files are commited to the Yama CMS repository.

## Repository organisation
### SSG Earthly manifests (./manifests/earthly)
`./manifests/earthly` holds directories for different Static Site Generators; each has an Earthfile, an .earthlyignore for this generator's specific files, and a .j2 jinja2 template.

The `Earthfile`s are generated from the `Earthfile.j2` templates, which simply stitches together the common bits from `/manifests/fragments` (nodejs-specific things, s3 upload via swiftclient...). We use this setup to reduce the burden of maintenance on our part. The fully templated manifests are still commited to the repository because we want these files to be standalone (for the same reason, we are not using the IMPORT feature of Earthly to dynamically import the `.earth` files.)

### CI system manifests (./manifests/ci-systems)
`./manifests/ci-systems` holds templates for different CI systems (Github Actions, Gitlab CI). They are mostly adapted from [Earthly's Vendor-Specific Guides](https://docs.earthly.dev/ci-integration/vendor-specific-guides), with a few changes (namely: authentication for OpenStack S3 uploads, notifying the Yama CMS platform of the pipeline status).

The pattern `{{CIEDITOR_REPLACEME_*}}` is automatically replaced by Yama CMS's CI-Editor tool when initializing a new static site. If you want to use these templates, you will need to replace them manually to match your OpenStack bucket configuration.

Note that `$YAMA_PREVIEW_CONTAINER_PASSWORD` and `$YAMA_PRODUCTION_CONTAINER_PASSWORD` reference a CI Secret and should **not** be inlined in the manifest file or commited to an `.env` file. The password is a sensitive value! If someone gets a hold of it, they will be able to upload/delete things on your bucket (and hence, your website).
All CI systems have a way to manage secrets to keep them out of the commited code. To learn more about it, search "CI secrets in <your CI platform>" on your favorite search engine.


## Misc
### Earthly targets
We aim to have the same set of targets in all of our Earthfiles to make it easy to try, develop and build using Static Site Generators. For more information, see the [Yama CMS documentation on Developing with Earthly](https://docs.yama-cms.com/docs/guide/build-deploy-earthly).


### Are there any pitfalls?
A few, which we hope will be resolved as YamaCMS and Earthly mature:

**Caching**: Caching can get confusing, and currently relies on storing/restoring container layers. It works out-of-the-box on dev machines (since you keep your layers between runs) and CI systems **if** you push images, but our Earthfiles do not.

The Earthly team themselves are exploring what can be done about it (mounting cache directories, uploading image layers to a Docker Registry...), and also develops [Earthly CI](https://docs.earthly.dev/earthly-cloud/earthly-ci), [Earthly Satellites](https://docs.earthly.dev/earthly-cloud/satellites) and [Earthly Cloud](https://docs.earthly.dev/earthly-cloud/overview), which boasts extreme speed thanks to caching and build isolation allowing to run concurrent build steps.


**The local dev server isn't properly closed when exiting**: Our Earthfiles have a `+dev` target that starts the SSG's devserver inside a container with the $PWD mounted. However, if you ctrl+C out of it, Earthly itself will stop but it **will not** stop the container: it will continue to run in the background. (The most proeminent way this can bit you is if you forget about it, then want to start another program/container on the same port - since the devserver is still running, your new program will crash as only one program can listen on a given port.)

Earthly is designed to run _build steps in isolation_, so using it to handle a _long-running_ dev server with _access to local files_ is a bit hacky. Still, it's pretty convenient to try out a generator quickly.

To stop the devserver started with `earthly +dev`, you will need to run `earthly +stop-dev` (or `docker stop earthly-dev-server` manually). You can check if the dev server is still running by running `docker ps`.


### Why do we use Earthly?
- It allows us to maintain one Earthfile for each static site generator, and one CI manifest for each CI system. Without it, we'd have to maintain a matrix of manifests for each generator on each CI system (Next.js+github, Next.js+gitlab, Nuxt.js+github... and so on). This drastically simplifies things on our end.
- The syntax and feature set is fairly straightforward and easy to understand, which reduces friction for devs unfamiliar with it.
- It's really good: it runs fast thanks to parallelization and caching, and build steps running on containers are perfect for our use cases.


### What is Earthly?
Tl;dr: "Write once, run everywhere" but for CI/CD pipelines.

[Earthly](https://earthly.dev/) is a layer of abstraction between your pipeline and the system it runs on (ie. your dev laptop, your coworkers' laptop, and your CI system). It uses Docker containers to achieve build isolation.

Earthfile's syntax is a mix between Dockerfiles and Makefiles, so they feel relatively familiar and are easily hackable. The `earthly` command interprets Earthfiles and orchestrates building containers, moving files, etc.

For example, here's how I run the build+upload pipeline from my dev machine:
`earthly --env-file ./.env.local --push +upload-site`

And here's how it runs in my .gitlab-ci.yaml:
```yaml
upload-prod:
  stage: build
  rules:
    - if: $CI_COMMIT_BRANCH == "yama/production"
  variables:
    # ... env vars declared here...
  script:
    - earthly --ci --push +upload-site
```
As you can see, it's almost the same command (minus how env vars are handled and CI-specific things). Both operate on the exact same Earthfile commited to that repository.


If you already know a bit about build systems but don't know Earthly, the [Earthly FAQ](https://earthly.dev/faq) is a good place to start.
